const toogle = document.querySelector('.theme-switch input[type="checkbox"]');
const theme = localStorage.getItem('theme');

if (theme) {
    document.documentElement.setAttribute('data-theme', theme);
    if (theme === 'dark') {
        toogle.checked = true;
    }
}
function switchTheme(e) {
    if (e.target.checked) {
        document.documentElement.setAttribute('data-theme', 'dark');
        localStorage.setItem('theme', 'dark');
    }
    else {
        document.documentElement.setAttribute('data-theme', 'light');
        localStorage.setItem('theme', 'light');
    }    
}
toogle.addEventListener('change', switchTheme, false);