from django.shortcuts import render, redirect
from .forms import StatForm
from .models import Stat
from . import forms
from datetime import datetime, date
from django.contrib import messages
from django.contrib.auth.models import User, auth

# Create your views here.
def login(request):
    if request.method=='POST':
        username=request.POST['username']
        password=request.POST['password']
        user=auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('index')
        else:
            if not username:
                return render(request, "login.html", {'confirm':'please fill your username'})
            return render(request, "login.html", {'confirm':'wrong username or password'})
    else:
        return render(request, "login.html")

def index(request):
    if request.method=='POST':
        form=StatForm(request.POST)
        if form.is_valid():
            status=form.cleaned_data['status']
            stat = Stat(status=status)
            stat.save()
            #Stat.objects.all().delete()
            return redirect('index')
        else:
            return render(request, "index.html",{'form':form})
    form=StatForm()
    forms=Stat.objects.all().order_by('date')
    return render(request, "index.html",{'form':form, 'forms':forms})

def registration(request):
    if request.method=='POST':
        username=request.POST['username']
        password=request.POST['password']
        password2=request.POST['password2']
        if not username:
            return render(request, "registration.html", {'confirm':'please fill your username'})
        if password==password2:
            if User.objects.filter(username=username).exists():
                return render(request, "registration.html", {'confirm':'username is not available'})
            else:
                if len(password)<8:
                    return render(request, "registration.html", {'confirm':'please fill your password'})
                user=User.objects.create_user(username=username, password=password)
                user.save()
                return render(request, "registration.html", {'confirm':'user has been created, go to login page'})
        return render(request, "registration.html", {'confirm':'password is not match'})
    else:
        return render(request, "registration.html")

def statRemove(request, stat_id):
    stat=Stat.objects.get(id=stat_id)
    stat.delete()
    return redirect('index')

def logout(request):
    auth.logout(request)
    return redirect('index')